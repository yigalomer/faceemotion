//
//  FaceDetectionViewModel.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 10/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import UIKit
import MBProgressHUD
import RxSwift
import RSKImageCropper

public protocol FaceDetectionViewModelDelegate: class {
    func didCompleteAnalayzing(croppedImage: UIImage?, emotionText: String)
    func didCompleteAnalayzingWithErrror(error: Error)
}

class FaceDetectionViewModel: NSObject, UINavigationControllerDelegate {

    private weak var viewController: UIViewController?
    private weak var imagePickerController: UIImagePickerController?
    private weak var delegate: FaceDetectionViewModelDelegate?
    private weak var cropDelegate: RSKImageCropViewControllerDelegate?

    private let disposeBag = DisposeBag()

    init(viewController: UIViewController) {
        self.viewController = viewController
        self.delegate = viewController as? FaceDetectionViewModelDelegate
        self.cropDelegate = viewController as? RSKImageCropViewControllerDelegate
    }

    func openImagePickerScreen(delegate: RSKImageCropViewControllerDelegate) {
        let viewController = UIImagePickerController()
        viewController.sourceType = .photoLibrary
        viewController.allowsEditing = false
        viewController.delegate = self
        self.imagePickerController = viewController
        self.viewController?.present(viewController, animated: true)
    }

    func openCameraScreen(delegate: RSKImageCropViewControllerDelegate) {
        let viewController = UIImagePickerController()
        viewController.sourceType = .camera
        viewController.allowsEditing = false
        viewController.cameraCaptureMode = .photo
        viewController.delegate = self
        self.imagePickerController = viewController
        self.viewController?.present(viewController, animated: true)
    }

    func closeImagePicker() {
        self.imagePickerController?.dismiss(animated: true)
        self.imagePickerController = nil

    }

    // MARK: - Select image alert
    func showSelectImageAlert() {
        let selecetImageAlert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)

        selecetImageAlert.addAction(UIAlertAction(title: "select_image_alert_action_take_photo".localized, style: .default) { [unowned self] _ in
            self.openCameraScreen(delegate: self.cropDelegate!)
        })
        selecetImageAlert.addAction(UIAlertAction(title: "select_image_alert_action_select_from_album".localized, style: .default) { [unowned self] _ in
            self.openImagePickerScreen(delegate: self.cropDelegate!)
        })

        selecetImageAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel))
        self.viewController?.present(selecetImageAlert, animated: true)
    }

    func startAnalayzingImage(selectedImageView: UIImageView?){
        showHUD()

        let imageData = selectedImageView?.image?.jpegData(compressionQuality: 0.8)

        if let imageData = imageData {
            RepositoryFacade.sharedInstance.sendFaceDetect(image: imageData)
                .subscribe(onNext: { (faces: [Face]) in
                    self.hideHUD()
                    if faces.count == 0 {
                        let error = APIError(message: "Could not detect a face", kind: .faceNotDetected, httpStatusCode: 200)
                        self.delegate?.didCompleteAnalayzingWithErrror(error: error)
                        return
                    }
                    // Limitation : if there are multiple faces in the image, it'll handle only the first
                    let face:Face = faces[0]
                    let emotion = face.faceAttributes?.emotion?.faceEmotion
                    let cropedRectangle = CGRect(x: face.faceRectangle!.left, y: face.faceRectangle!.top, width: face.faceRectangle!.width, height: face.faceRectangle!.height)

                    let cropedImage: UIImage? = selectedImageView?.image?.crop(toRect: cropedRectangle, viewWidth: 50, viewHeight: 50)

                    self.delegate?.didCompleteAnalayzing(croppedImage: cropedImage, emotionText: emotion!)

                }, onError: { (error: Error) in
                    print("Error sending Detect face request : \(error.localizedDescription)")
                    self.hideHUD()
                    self.delegate?.didCompleteAnalayzingWithErrror(error: error)
                }).disposed(by: self.disposeBag)
        }
    }

    func showHUD() {
        DispatchQueue.main.async {
            if let view = self.viewController?.view {
                let hud = MBProgressHUD.showAdded(to: view, animated: true)
                hud.mode = .indeterminate
                hud.label.text = "analayzing_image".localized
            }
        }
    }

    func hideHUD() {
        DispatchQueue.main.async {
            if let view = self.viewController?.view {
                MBProgressHUD.hide(for: view, animated: true)
            }
        }
    }
}

// MARK: - UIImagePickerControllerDelegate
extension FaceDetectionViewModel: UIImagePickerControllerDelegate {

    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.imagePickerController = nil
        picker.dismiss(animated: true)
    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {

        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            let viewController = RSKImageCropViewController(image: image, cropMode: .square)
            viewController.delegate = self.cropDelegate
            picker.pushViewController(viewController, animated: true)
        } else {
            self.imagePickerController = nil
        }
    }
}
