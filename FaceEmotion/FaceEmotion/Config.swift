//
//  Config.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import UIKit

var TIMEOUT_INTERVAL_FOR_REQUEST: Double = 15

public enum Config: String {
    case FaceSubscriptionKey = "c4844f5da1a3452594e8e14cfc56fd3a"
    case FaceEndpointUrl = "https://westcentralus.api.cognitive.microsoft.com/face/v1.0"

}
