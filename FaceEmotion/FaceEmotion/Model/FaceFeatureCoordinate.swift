//
//  PersonFace.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import UIKit

struct FaceFeatureCoordinate: Codable {
    var x: Float
    var y: Float

    enum CodingKeys: String, CodingKey {
        case x = "x"
        case y = "y"
    }
}
