//
//  PersonFace.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import UIKit

struct FaceAttributes: Codable {
    //var age: Int
    //var smile: Int
    //var gender: String
    //var glasses: String
    //var facialHair: FacialHair?
    //var headPose: FaceHeadPose?
    var emotion: FaceEmotion?

    enum CodingKeys: String, CodingKey {
        //case age = "age"
        case emotion = "emotion"
    }
    //    var hair: Hair?
    //    var makeup: Makeup?
    //    var occlusion: Occlusion?
    //    var accessories: Accessories?
    //    var blur: Blur?
    //    var exposure: Exposure?
    //    var noise: Noise?

}
