//
//  PersonFace.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import UIKit

struct FaceLandmarks: Codable {
    var pupilLeft: FaceFeatureCoordinate?
    var pupilRight: FaceFeatureCoordinate?
    var noseTip: FaceFeatureCoordinate?
    var mouthLeft: FaceFeatureCoordinate?
    var mouthRight: FaceFeatureCoordinate?
    var eyebrowLeftOuter: FaceFeatureCoordinate?
    var eyebrowLeftInner: FaceFeatureCoordinate?
    var eyeLeftOuter: FaceFeatureCoordinate?
    var eyeLeftTop: FaceFeatureCoordinate?
    var eyeLeftBottom: FaceFeatureCoordinate?
    var eyeLeftInner: FaceFeatureCoordinate?
    var eyebrowRightOuter: FaceFeatureCoordinate?
    var eyebrowRightInner: FaceFeatureCoordinate?
    var eyeRightInner: FaceFeatureCoordinate?
    var eyeRightTop: FaceFeatureCoordinate?
    var eyeRightBottom: FaceFeatureCoordinate?
    var eyeRightOuter: FaceFeatureCoordinate?
    var noseRootLeft: FaceFeatureCoordinate?
    var noseRootRight: FaceFeatureCoordinate?
    var noseLeftAlarTop: FaceFeatureCoordinate?
    var noseRightAlarTop: FaceFeatureCoordinate?
    var noseLeftAlarOutTip: FaceFeatureCoordinate?
    var noseRightAlarOutTip: FaceFeatureCoordinate?
    var upperLipTop: FaceFeatureCoordinate?
    var upperLipBottom: FaceFeatureCoordinate?
    var underLipTop: FaceFeatureCoordinate?
    var underLipBottom: FaceFeatureCoordinate?

    enum CodingKeys: String, CodingKey {

        case pupilRight = "pupilRight"
        case noseTip = "noseTip"
        case mouthLeft = "mouthLeft"
        case mouthRight = "mouthRight"
        case eyebrowLeftOuter = "eyebrowLeftOuter"
        case eyebrowLeftInner = "eyebrowLeftInner"
        case eyeLeftOuter = "eyeLeftOuter"
        case eyeLeftTop = "eyeLeftTop"
        case eyeLeftBottom = "eyeLeftBottom"
        case eyeLeftInner = "eyeLeftInner"
        case eyebrowRightOuter = "eyebrowRightOuter"
        case eyebrowRightInner = "eyebrowRightInner"
        case eyeRightInner = "eyeRightInner"
        case eyeRightTop = "eyeRightTop"
        case eyeRightBottom = "eyeRightBottom"
        case eyeRightOuter = "eyeRightOuter"
        case noseRootLeft = "noseRootLeft"
        case noseRootRight = "noseRootRight"
        case noseLeftAlarTop = "noseLeftAlarTop"
        case noseLeftAlarOutTip = "noseLeftAlarOutTip"
        case noseRightAlarTop = "noseRightAlarTop"
        case noseRightAlarOutTip = "noseRightAlarOutTip"
        case upperLipTop = "upperLipTop"
        case upperLipBottom = "upperLipBottom"
        case underLipTop = "underLipTop"
        case underLipBottom = "underLipBottom"
    }
}
