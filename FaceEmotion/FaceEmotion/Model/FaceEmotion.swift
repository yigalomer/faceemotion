//
//  PersonFace.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import UIKit

struct FaceEmotion: Codable {
    var anger: Float
    var contempt: Float
    var disgust: Float
    var fear: Float
    var happiness: Float
    var neutral: Float
    var sadness: Float
    var surprise: Float

    enum CodingKeys: String, CodingKey {
        case anger = "anger"
        case contempt = "contempt"
        case disgust = "disgust"
        case fear = "fear"
        case happiness = "happiness"
        case neutral = "neutral"
        case sadness = "sadness"
        case surprise = "surprise"
    }

    var faceEmotion: String {
        get {
            var mostEmotion = "anger"
            var mostEmotionValue = anger
            if self.contempt > mostEmotionValue {
                mostEmotion = "contempt"
                mostEmotionValue = contempt
            }
            if self.disgust > mostEmotionValue {
                mostEmotion = "disgust"
                mostEmotionValue = disgust
            }
            if self.fear > mostEmotionValue {
                mostEmotion = "fear"
                mostEmotionValue = fear
            }
            if self.happiness > mostEmotionValue {
                mostEmotion = "happiness"
                mostEmotionValue = happiness
            }
            if self.neutral > mostEmotionValue {
                mostEmotion = "neutral"
                mostEmotionValue = neutral
            }
            if self.sadness > mostEmotionValue {
                mostEmotion = "sadness"
                mostEmotionValue = sadness
            }
            if self.surprise > mostEmotionValue {
                mostEmotion = "surprise"
                mostEmotionValue = surprise
            }
            return mostEmotion
        }
    }

}
