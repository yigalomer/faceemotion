//
//  PersonFace.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import UIKit

struct FaceRectangle: Codable {

    var top: Int
    var left: Int
    var width: Int
    var height: Int

    enum CodingKeys: String, CodingKey {
        case top = "top"
        case left = "left"
        case width = "width"
        case height = "height"
    }
}
