//
//  PersonFace.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import UIKit

public struct Face: Codable {
    var faceId:String
    var faceRectangle: FaceRectangle?
    var faceLandmarks: FaceLandmarks?
    var faceAttributes: FaceAttributes?
    
    enum CodingKeys: String, CodingKey {
        case faceId = "faceId"
        case faceRectangle = "faceRectangle"
        case faceLandmarks = "faceLandmarks"
        case faceAttributes = "faceAttributes"
    }
}
