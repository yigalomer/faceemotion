//
//  FaceDetectionViewController.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 Figure8. All rights reserved.
//

import UIKit
import RxSwift
import SnapKit
import MBProgressHUD
import RSKImageCropper

class FaceDetectionViewController: UIViewController {

    private var faceDetectionViewModel: FaceDetectionViewModel?
    private var backgroundView: UIImageView!
    private var subtitleLabel: UILabel!
    private var selectedImageView: UIImageView!
    private var titleLabel: UILabel!
    private var selectImageButton: UIButton!
    private var analyzeButton: UIButton!
    private var emotionLabel: UILabel!
    private var croppedAnalayzedImageView: UIImageView!

    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        faceDetectionViewModel = FaceDetectionViewModel(viewController: self)
        setupView()
        setupBackgroundView()
        setupTitleView()
        setupSubtitleLabel()
        setupSelectImageButton()
        setupAnalyzeButton()
        setupSelectedImageView()
        setupEmotionText()
        setupAnalayzedImageView()
        setInitialViewState()
        updateViewConstraints()
    }
}

// MARK: - Build view
extension FaceDetectionViewController {

    private func setupView() {
        self.view.backgroundColor = UIColor.clear
        self.navigationController?.isNavigationBarHidden = true
    }

    private func setupBackgroundView() {
        let view = UIImageView()
        view.image = #imageLiteral(resourceName: "face_detect_bg")
        view.alpha = 0.8
        view.contentMode = .scaleAspectFill
        self.view.addSubview(view)
        self.backgroundView = view
    }

    private func setupTitleView() {
        let view = UILabel()
        view.textColor = UIColor.white
        view.text = "face_detection".localized
        view.adjustsFontSizeToFitWidth = true
        view.font = UIFont(name: "HelveticaNeue-Bold", size: 28)
        view.textAlignment = .center
        self.view.addSubview(view)
        self.titleLabel = view
    }

    private func setupSubtitleLabel() {
        let view = UILabel()
        view.textColor = UIColor.white
        view.text = "subtitle_text".localized
        view.textAlignment = .center
        view.font = UIFont(name: "HelveticaNeue", size: 18)
        view.numberOfLines = 5
        self.view.addSubview(view)
        self.subtitleLabel = view
    }

    private func setupSelectImageButton() {
        let view = UIButton(type: .custom)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("select_image".localized, for: .normal)
        view.setTitleColor(UIColor.white, for: .normal)
        view.backgroundColor = UIColor(hexString: "#009edb")
        view.addTarget(self, action: #selector(onSelectImageButtonClick(sender:)), for: .touchUpInside)
        view.layer.cornerRadius = 7
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.white.cgColor
        self.view.addSubview(view)
        self.selectImageButton = view
    }

    private func setupAnalyzeButton() {
        let view = UIButton(type: .custom)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.setTitle("analayze_image".localized, for: .normal)
        view.setTitleColor(UIColor.white, for: .normal)
        view.backgroundColor = UIColor.lightGray
        view.layer.cornerRadius = 7
        view.layer.borderWidth = 2
        view.backgroundColor = UIColor(hexString: "#009edb")
        view.layer.borderColor = UIColor.white.cgColor
        view.addTarget(self, action: #selector(onAnalyzeImageButtonClick(sender:)), for: .touchUpInside)
        self.view.addSubview(view)
        self.analyzeButton = view
    }
    private func setupEmotionText() {
        let view = UILabel()
        view.textColor = UIColor.white
        view.textAlignment = .center
        view.font = UIFont(name: "HelveticaNeue", size: 23)
        view.numberOfLines = 1
        self.view.addSubview(view)
        self.emotionLabel = view
    }
    private func setupSelectedImageView() {
        let view = UIImageView()
        view.frame.size.height = 70
        view.layer.borderWidth = 4
        view.layer.cornerRadius = 9
        view.layer.borderColor = UIColor.white.cgColor
        self.view.addSubview(view)
        self.selectedImageView = view
    }
    private func setupAnalayzedImageView() {
        let view = UIImageView()
        view.frame.size.height = 70
        view.frame.size.width = 70
        view.layer.borderWidth = 2
        view.layer.cornerRadius = 3
        view.layer.borderColor = UIColor.white.cgColor
        self.view.addSubview(view)
        self.croppedAnalayzedImageView = view
    }

    private func setCroppedImageView(image:UIImage? = nil) {
        self.croppedAnalayzedImageView.image = image
    }

    private func setInitialViewState() {
        self.selectImageButton.isHidden = false
        self.analyzeButton.isHidden = true
        self.emotionLabel.isHidden = true
        self.selectedImageView.isHidden = true
        self.croppedAnalayzedImageView.isHidden = true
    }
    private func setImageSelectedViewState() {
        self.selectedImageView.isHidden = false
        self.selectedImageView.backgroundColor = UIColor.white
        self.emotionLabel.isHidden = true
        self.analyzeButton.isHidden = false
        self.croppedAnalayzedImageView.isHidden = true
    }
    private func setAnalyzingViewState() {
        self.selectedImageView.isHidden = false
        self.emotionLabel.isHidden = false
        self.analyzeButton.isHidden = true
        self.croppedAnalayzedImageView.isHidden = true
    }
    private func setAnalyzeCopmletedViewState(isError: Bool) {
        self.selectedImageView.isHidden = false
        self.emotionLabel.isHidden = false
        self.analyzeButton.isHidden = true
        isError ? (self.croppedAnalayzedImageView.isHidden = true) : (self.croppedAnalayzedImageView.isHidden = false)
    }

    override func updateViewConstraints() {
        self.titleLabel.snp.remakeConstraints { (make: ConstraintMaker) in
            make.leading.equalToSuperview().offset(20)
            make.trailing.equalToSuperview().offset(-20)
            make.top.equalToSuperview().offset(60)
        }
        self.subtitleLabel.snp.remakeConstraints { (make: ConstraintMaker) in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(self.titleLabel.snp.bottom).offset(20)
        }
        self.selectImageButton.snp.remakeConstraints { (make: ConstraintMaker) in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(self.subtitleLabel.snp.bottom).offset(40)
        }
        self.selectedImageView.snp.remakeConstraints { (make: ConstraintMaker) in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(self.selectImageButton.snp.bottom).offset(50)
            make.bottom.equalToSuperview().offset(-280)
        }
        self.analyzeButton.snp.remakeConstraints { (make: ConstraintMaker) in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(self.selectedImageView.snp.bottom).offset(100)
        }
        self.emotionLabel.snp.remakeConstraints { (make: ConstraintMaker) in
            make.leading.equalToSuperview().offset(40)
            make.trailing.equalToSuperview().offset(-40)
            make.top.equalTo(self.selectedImageView.snp.bottom).offset(100)
        }
        if (  self.croppedAnalayzedImageView != nil) {
            self.croppedAnalayzedImageView.snp.remakeConstraints { (make: ConstraintMaker) in
                make.height.equalTo(70)
                make.width.equalTo(70)
                make.centerX.equalToSuperview()
                make.top.equalTo(self.emotionLabel.snp.bottom).offset(10)
            }
        }
        self.backgroundView.snp.remakeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        super.updateViewConstraints()
    }

    @objc private func onSelectImageButtonClick(sender: UIButton) {
        faceDetectionViewModel?.showSelectImageAlert()
    }

    @objc private func onAnalyzeImageButtonClick(sender: UIButton) {
        setAnalyzingViewState()
        faceDetectionViewModel?.startAnalayzingImage(selectedImageView: selectedImageView)
    }
}

// MARK: - RSKImageCropViewControllerDelegate
extension FaceDetectionViewController : RSKImageCropViewControllerDelegate {

    func imageCropViewControllerDidCancelCrop(_ controller: RSKImageCropViewController) {
        self.faceDetectionViewModel?.closeImagePicker()
    }

    func imageCropViewController(_ controller: RSKImageCropViewController, didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect, rotationAngle: CGFloat) {
        DispatchQueue.main.async {
            self.setImageSelectedViewState()
            self.selectedImageView.image = croppedImage
            self.faceDetectionViewModel?.closeImagePicker()
        }
    }
}

// MARK: - FaceDetectionViewModelDelegate

extension FaceDetectionViewController : FaceDetectionViewModelDelegate {

    func didCompleteAnalayzing(croppedImage: UIImage?, emotionText: String){
        DispatchQueue.main.async {
            self.setAnalyzeCopmletedViewState(isError:false)
            self.emotionLabel.text = "Emotion: " + emotionText
            if croppedImage?.imageOrientation != .up {
                let imageWithFixedOrientation = UIImage(cgImage: (croppedImage?.cgImage!)!, scale: croppedImage!.scale, orientation: .up)
                self.setCroppedImageView(image:imageWithFixedOrientation)
            }
            else{
                self.setCroppedImageView(image:croppedImage)
            }
            self.updateViewConstraints()
        }
    }

    func didCompleteAnalayzingWithErrror(error: Error) {
        DispatchQueue.main.async {
            self.showToast(message: error.localizedDescription, font: UIFont(name: "HelveticaNeue", size: 18)!, width: 300)
            self.setAnalyzeCopmletedViewState(isError:true)
            self.updateViewConstraints()
        }
    }
}

