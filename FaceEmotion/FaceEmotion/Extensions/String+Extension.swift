//  String+Extension.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 10/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//
import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
}
