//
//  UIImage+Crop.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//
import UIKit

extension UIImage {

    func crop(toRect cropRect: CGRect, viewWidth: CGFloat, viewHeight: CGFloat) -> UIImage? {

        let imageViewScale = self.scale
        let cropZone = CGRect(x:cropRect.origin.x * imageViewScale,
                              y:cropRect.origin.y * imageViewScale,
                              width:cropRect.size.width * imageViewScale,
                              height:cropRect.size.height * imageViewScale)

        guard let cutImageRef: CGImage = self.cgImage?.cropping(to:cropZone) else { return nil }
        let croppedImage: UIImage = UIImage(cgImage: cutImageRef, scale: self.scale, orientation:self.imageOrientation)
        return croppedImage
    }
}
