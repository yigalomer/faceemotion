//
//  RepositoryFacade.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 08/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import UIKit
import Foundation
import RxSwift
import RxCocoa

class RepositoryFacade {

    private let api: FaceAPI
    private let faceDetectRepository: FaceDetectRepository
    public static let sharedInstance: RepositoryFacade = RepositoryFacade()

    private init() {
        let sessionConfiguration: URLSessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.timeoutIntervalForRequest = TIMEOUT_INTERVAL_FOR_REQUEST

        self.api = FaceAPIImpl(sessionConfiguration: sessionConfiguration)
        self.faceDetectRepository = FaceDetectRepository(api: self.api)
    }

    public func sendFaceDetect(image: Data) -> Observable<[Face]> {
        return self.faceDetectRepository.sendFaceDetect(image: image)
    }
}
