//
//  FaceDetectRepository.swift
//  Repositories
//
//  Created by Yigal Omer on 16/05/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import RxSwift

import RxCocoa

enum MPOFaceAttributeType: String {
    case MPOFaceAttributeTypeAge = "age"
    case MPOFaceAttributeTypeGender = "gender"
    case MPOFaceAttributeTypeSmile = "smile"
    case MPOFaceAttributeTypeGlasses = "glasses"
    case MPOFaceAttributeTypeFacialHair = "facialHair"
    case MPOFaceAttributeTypeHeadPose = "headPose"
    case MPOFaceAttributeTypeEmotion = "emotion"
    case MPOFaceAttributeTypeHair = "hair"
    case MPOFaceAttributeTypeMakeup = "makeup"
    case MPOFaceAttributeTypeOcclusion = "occlusion"
    case MPOFaceAttributeTypeAccessories = "accessories"
    case MPOFaceAttributeTypeBlur = "blur"
    case MPOFaceAttributeTypeExposure = "exposure"
    case MPOFaceAttributeTypeNoise = "noise"
} 

public class FaceDetectRepository {

    private var api: FaceAPI

    init(api: FaceAPI) {
        self.api = api
    }

    public func sendFaceDetect(image: Data)  -> Observable<[Face]> {
        let apiCall = APICall(endpoint: FaceDetectEndPoint.detect)
        
        apiCall.add(header:"Ocp-Apim-Subscription-Key", value: Config.FaceSubscriptionKey.rawValue)
        apiCall.add(header:"Content-Type", value: "application/octet-stream")
        apiCall.add(param: "returnFaceId", value: "true")
        apiCall.add(param: "returnFaceLandmarks", value: "true")
        apiCall.add(httpBody: image)

        // let attributesArray :String = MPOFaceAttributeType.MPOFaceAttributeTypeGender.rawValue + "," + MPOFaceAttributeType.MPOFaceAttributeTypeAge.rawValue + "," +  MPOFaceAttributeType.MPOFaceAttributeTypeHair.rawValue + "," + MPOFaceAttributeType.MPOFaceAttributeTypeEmotion.rawValue

        let attributes :String = MPOFaceAttributeType.MPOFaceAttributeTypeEmotion.rawValue
        apiCall.add(param: "returnFaceAttributes", value: attributes)


        return api.detectFaceRequest(apiCall).flatMap { (json) -> Observable<[Face]> in
            if let jsonString: String = json as? String {
                var faces:[Face]
                if let jsonDataTemp = jsonString.data(using: .utf8) {
                    let decoder = JSONDecoder()
                    do {
                        faces = try decoder.decode([Face].self, from: jsonDataTemp)
                        //print(faces)
                        return Observable<[Face]>.just(faces)
                    } catch {
                        print(error.localizedDescription)
                        return Observable.error(error)
                    }
                }
            } else {
                return Observable.error(APIError(message: "Invalid data at top level of JSON. Expected `JSON String`, got \(type(of: json))", kind: .invalidResponse, httpStatusCode: 200))
            }
            return Observable.error(APIError(message: "Unknown error", kind: .unknown))
        }
    }
}

