//
//  APIImpl.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 10/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

public class FaceAPIImpl: FaceAPI {

    public var errorHandler: ErrorHandler?
    private let sessionManager: SessionManager

    required public init( sessionConfiguration: URLSessionConfiguration ) {
        self.sessionManager = Alamofire.SessionManager(configuration: sessionConfiguration)
    }

    public func detectFaceRequest(_ apiCall: APICall) -> Observable<Any>{
        return self.call(with: apiCall).flatMap { (json) -> Observable<Any> in
            if let jsonString: String = json as? String {
                return Observable<Any>.just(jsonString)
            } else {
                return Observable.error(APIError(message: "Invalid data. Expected `JSON String`, got \(type(of: json))", kind: .invalidResponse, httpStatusCode: 200))
            }
        }
    }
}


extension FaceAPIImpl {

    // MARK: Private
    private func call(with apiCall: APICall) -> Observable<Any> {
        var urlRequest = createUrlRequest(for: apiCall)

        return executeAPICall(urlRequest).rx.responseString()
            .do(onNext: { (response: DataResponse<String>) in
                switch response.result {
                case .success(let value):
                    if let requestUrl = urlRequest.url?.description {
                        print("Got response for \(apiCall.endpoint.info.method.rawValue) request \(requestUrl)  : response = \(value))")
                    }
                case .failure(let error):
                    print("Error: \(error)")
                    var errorString: String = error.localizedDescription
                    if let data = response.data {
                        do {
                            if let parsedData = try JSONSerialization.jsonObject(with: data) as? [String: Any], let errorStr = parsedData["message"] as? String {
                                errorString = errorStr
                            } else {
                                errorString = ""
                            }
                            print("Error description from server: \(errorString)")
                        } catch {
                            print("Cannot parse server error message: \(error.localizedDescription)")
                        }
                    }
                }
            }, onError: { (error: Error) in
                print("onError Error : \(error)")
            })
            .map { (response: DataResponse<String>) in
                if let value = response.result.value {
                    return value
                } else {
                    return [String: Any]()
                }
        }
    }

    private func createUrlRequest(for apiCall: APICall) -> URLRequest {
        let url = URL.init(string: Config.FaceEndpointUrl.rawValue)
        var urlRequest = URLRequest(url:((url?.appendingPathComponent(apiCall.endpoint.info.path))!))
        urlRequest.httpMethod = apiCall.endpoint.info.method.rawValue
        urlRequest.httpBody = apiCall.httpBody

        if !apiCall.allParameters.isEmpty {
            do {
                let urlEncoding = URLEncoding(destination: .queryString, arrayEncoding: .noBrackets, boolEncoding: .literal)
                urlRequest = try urlEncoding.encode(urlRequest, with: apiCall.allParameters)
            } catch {
                print("Can not encode api call parameters to urlRequest. Error: \(error)")
            }
        }

        apiCall.allHttpHeaders.forEach { (key: String, value: String) in
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }

        return urlRequest
    }

    private func executeAPICall(_ urlRequest: URLRequest) -> DataRequest {
        return sessionManager
            .request(urlRequest)
            .validate({ (optionalRequest, response, optionalData) -> Request.ValidationResult in
                if let _ = response.allHeaderFields["Content-Type"] as? String,
                    200...299 ~= response.statusCode {
                    return .success
                }
                var errorString: String = "Unexpected error"
                if let data = optionalData {
                    do {
                        if let parsedData = try JSONSerialization.jsonObject(with: data) as? [String: Any], let errorStr = parsedData["message"] as? String {
                            errorString = errorStr
                        }
                        print("Error description from server: \(errorString)")
                    } catch {
                        print("Cannot parse server error message: \(error.localizedDescription)")
                    }
                }
                let error = APIError(message: errorString, kind: .invalidResponse, httpStatusCode: response.statusCode)
                return .failure(error)
            })
    }

}

