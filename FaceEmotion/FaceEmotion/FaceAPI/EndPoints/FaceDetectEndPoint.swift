//
//  FaceDetectEndPoint.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 10/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//
import Foundation

public enum FaceDetectEndPoint: APIEndPoint {

    case detect

    public var info: APIEndPointInfo {
        switch self {
        case .detect:
            return (.post, "/detect")
        }
    }
}


