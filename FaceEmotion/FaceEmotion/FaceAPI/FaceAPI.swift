//
//  API.swift
//  FaceEmotion
//
//  Created by Yigal Omer on 10/07/2019.
//  Copyright © 2019 YigalOmer. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire

public protocol BaseRequestInterceptorType: class {}

public protocol RequestInterceptorType: (RequestRetrier & RequestAdapter & BaseRequestInterceptorType) {
     var userId: String { get set }
     var errorHandler: ErrorHandler? { get set }
}

public protocol FaceAPI: class {

    var errorHandler: ErrorHandler? { get set }

    func detectFaceRequest(_ apiCall: APICall) -> Observable<Any>
}
